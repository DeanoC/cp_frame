package cp_site;

import framework.utils.Json;
import framework.utils.UnitTester;
import framework.utils.Config;
#if nodejs
    import framework.js.Lib;
    import framework.js.Node;
    import framework.js.Node.NodeHttp;
#end
import framework.utils.AutoRestart;

/**
 * 
 * @author Deano Calver
 */

class Main {
    
    static function main() {
#if nodejs
        // read a JSON config script
        var cfgText = Node.fs.readFileSync( "./config.json" );
        Config.config = Json.decode( cfgText );
#end

        // log some basic process stuff
        if( Config.config.verbose > 0 ) {
            LogProcess();
        }
    
        // fire unit tests
#if unittest
        UnitTester.addTestSubject( new db.mongo.Tests() );
        UnitTester.addTestSubject( new net.RouterTests() );
        UnitTester.addTestSubject( new net.RestfulTests() );
#end
        
        // load crypto keys and certs
/*      var privatekey = Node.fs.readFileSync("./ssl/transmediagames_com-key.pem");  
        var certificate = Node.fs.readFileSync("./ssl/transmediagames_com-cert.pem");  
        var ca_bundle = Node.fs.readFileSync("./ssl/transmediagames_com-ca-bundle.pem");  
        
        var credentials =  { key: privatekey, cert: certificate, ca: ca_bundle };       
*/
#if reset_userdb
        User.reset_userdb();
#end
#if unittest
        var test = new User();
        test.tryLogin( "TestAccount", "!2ASczfe%$gtb4j", function( res : Bool, doc ) {
            trace( "Test Login res : " + res );
        });
#end
/*        
        // create routers
        var secRouter = new SecureRoutes();
        var insecRouter = new InsecureRoutes();
      
        // create https server
        Node.https.createServer( untyped credentials,
            function ( request : NodeHttpServerReq, response : NodeHttpServerResp) {
                secRouter.route( request, response );
            }
        ).listen( Config.config.port, Config.config.host );

        // create http server
        Node.http.createServer( function ( request : NodeHttpServerReq, response : NodeHttpServerResp) {
                insecRouter.route( request, response );
            }
        ).listen( Config.config.insecureport, Config.config.host );
*/
        
#if nodejs
#if unittest
        Node.process.on( "exit", function() {
            UnitTester.endCheck();
        });
#end
#end
    }

    static function LogProcess() {
#if nodejs
        var process = Node.process;
        trace( 'Node.js Information' );
        trace( '\tVersion: ' + process.version );
        trace( '\tPlatform: ' + process.platform );
        trace( '\tPrefix: ' + process.installPrefix );
        trace( '\tCWD: ' + process.cwd() );
        trace( '\tuid.gid: ' + process.getuid() + '.' + process.getgid() );
        trace( '\tnow: ' + Date.now() );
#end
    }
    
}

package framework;

import haxe.PosInfos;
import utils.UnitTester;

/**
 * ...
 * @author Deano Calver
 */

class Debug {
#if debug
	static public var assertProc : Bool -> String -> PosInfos -> Bool = defaultAssertProc;
	static public function assert( cond : Bool, ?msg : String, ?pos : PosInfos ) : Bool
		return assertProc(cond, msg, pos)

	private static function defaultAssertProc( cond, msg, pos) {
		if (cond)
			return true;
        msg = msg == null? "assert failed" : "assert failed: " + msg;
//		haxe.Log.trace(msg, pos);
#if unittest
		UnitTester.failz( pos );
#end
#if js
// TODO        if (confirm(msg + "\nPress OK to run debugger, Cancel to continue:")) 
//		{
//               untyped __js__("debugger");
//        }
#end

		throw new String( "ASSERT FAILED: " + msg );
		return false;
	}
#else
	static public inline function assert( cond : Bool, ?msg : String, ?pos : PosInfos ) : Bool return true
#end
} 
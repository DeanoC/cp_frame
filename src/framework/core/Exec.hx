package framework.core;
import flash.Lib;
import haxe.Timer;

class SimpleTimer extends Timer {
	public function new( time_ms : Int, fun : Void -> Void ) {
		super( time_ms );
	}
	private var func : Void -> Void;
	public override function run() {
		func();
	}
}
/**
 * ...
 * @author Deano Calver
 */

class Exec {
	public inline static var useThreads : Bool = false;

	// public singleton get function (inits on first use, so call in setup to pre-setup)
	public static function get() : Exec {
		if ( theExec == null ) {
			theExec = new Exec();
		}
		return theExec;
	}
	
	// A job is a function that takes an object specified by the caller (2nd param)
	// and performs some operations taking no more than 1 frame (should be a lot less!)
	// if it wants executing again, it return true else if finished false
	public function addJob( func : Dynamic -> Bool, ?object : Dynamic ) {
		addJobAtAbsolute( tickCount, func, object );
		
	}
	public function addDelayedJob( frameDelay : Int, func : Dynamic -> Bool, ?object : Dynamic ) {
		addJobAtAbsolute( tickCount + frameDelay, func, object );
	}
	
	private function addJobAtAbsolute( frameToRun : Int, func : Dynamic -> Bool, ?object : Dynamic ) {
		Debug.assert( func != null );
		nextJobList.add( {func:func, object:object, when2run: frameToRun } );
	}
	
	// should not be needed in most cases, at the mo only in the cpp unit test
	// driver and thats a bug I will fix at some point
	public function forceTick() {
		updateExec();
	}
	private function updateExec() {
		Debug.assert( currentJobList.isEmpty() );
		var tJL = currentJobList;
		currentJobList = nextJobList;
		nextJobList = tJL;
		if ( useThreads == false ) {
			for ( i in currentJobList ) {
				if( i.when2run <= tickCount ) {
					var goAgain = i.func(i.object);
					if ( goAgain == true ) {
						addJob( i.func, i.object );
					}
				} else {
					addJobAtAbsolute( i.when2run, i.func, i.object );
				}
			}
		} else {
			Debug.assert( false, "TODO multi-threading" );
		}
		currentJobList.clear();
		tickCount++;
	}
	
	private static var theExec : Exec;
	private var tickCount : Int;
	private var currentJobList : List < { func: Dynamic -> Bool, object:Dynamic, when2run : Int } >;
	private var nextJobList : List < { func: Dynamic -> Bool, object:Dynamic, when2run : Int } >;

	// flash doesn't support threading yet, so no point
	// exposing a normal thread model, instead we use a job system
	// ala SPU and Apple grand central station. Very basic but should
	// do. Each job should perform a small portion of work and then
	// return false if its finished or true to be recalled in the future
	// it will be restarted from the beginning, so its not even co-operative
	// multitasking sorry.
	// on platform that support multi-threading it wil support it via
	// multiple simulatious jobs being executed at the same time, also
	// they will run in the backgroudn compared to the flash model.
	// this will make them MUCH faster than flash on multi-cores
	// no job should ever take longer than 1 frame for the system to remain
	// responsive on flash targets
		
	private function new() {
		currentJobList = new List();
		nextJobList = new List();
		tickCount = 0;

		var me = this;
#if (flash || js || cpp)
		// add a once per start of frame call to updateExec, poor mans threading...
		flash.Lib.current.addEventListener ( flash.events.Event.ENTER_FRAME, function(_) { me.updateExec(); } );
#else
		timer = new SimpleTimer(33, function() { me.updateExec(); } );
#end
	}
#if !(flash || js)
	private var timer : SimpleTimer;
#end
	
}
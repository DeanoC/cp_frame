package framework.core;
import flash.display.Loader;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.URLRequestHeader;
import framework.utils.Json;

/**
 * Enumeration of the all the statuses a completion callback can be passed
 * Failed indicates that ithe resource loader cannot load or continue for some
 * reason.
 */
enum CompletionStatus {
	Failed;
	Loaded;
}

/**
 * The resource loader handles the load and processing directly, except for Raw* types
 */
enum ResourceType {
	RawText;
	RawBinary;
	Image;
	UrlParameters;
}

private typedef  ResourceItem = {
	var fileName : String;
	var type : ResourceType;
	var content : Dynamic;
	var method : String;
	var params : URLVariables;
	var onCompleteCallBack : List< CompletionStatus -> Dynamic -> Void>;
}

/**
 * ResourceLoader is a singleton interface abstracting a resource loader queue
 * All operations are async but are thread safe from the users point of view
 * Completation callback however may be called on any thread.
 * @author Deano Calver
 */
class ResourceLoader {
	// public singleton get function (inits on first use, so call in setup to pre-setup)
	public static function get() : ResourceLoader {
		if ( theLoader == null ) {
			theLoader = new ResourceLoader();
		}
		return theLoader;
	}
	
	// set the params to be set with each url (after the ?) this allows clientId or
	// other global ids to be send without constantly adding them to the load command
	public function setConstantParams( params : Array<String>) {		
		constantParams = new URLVariables();
		for ( param in params ) {
			var keyValue = param.split('=');
			if( keyValue.length == 2 ) {
				Reflect.setField( constantParams, keyValue[0], keyValue[1] );
			}
		}
	}
	
	
	// loads the specified fileName of the given type, optional onComplete handler will be called
	// If no onComplete handler is given, it will act as a basic file only pre-load
	// onComplete is passed the status and the content if loaded or null if failed
	public function load( type : ResourceType, name : String, ?resourceParams : Array<String>, ?onComplete : CompletionStatus -> Dynamic -> Void ) {
		if ( onComplete == null ) {
			onComplete = defaultCompletionHandler;
		}
		var resParams = new URLVariables();
		if( resourceParams != null ) {
			for ( param in resourceParams ) {
				var keyValue = param.split('=');
				if( keyValue.length == 2 ) {
					Reflect.setField( resParams, keyValue[0], keyValue[1] );
				}
			}	
		}

		var callmes = new List();
		callmes.add( onComplete );
		loadQueue.push( { fileName: name, type: type, content: null, params: resParams, method: URLRequestMethod.GET, onCompleteCallBack : callmes } );
	}

	public function head( type : ResourceType, name : String, ?resourceParams : Array<String>, onComplete : CompletionStatus -> Dynamic -> Void ) {
		if ( onComplete == null ) {
			onComplete = defaultCompletionHandler;
		}

		var resParams = new URLVariables();
		if( resourceParams != null ) {
			for ( param in resourceParams ) {
				var keyValue = param.split('=');
				if( keyValue.length == 2 ) {
					Reflect.setField( resParams, keyValue[0], keyValue[1] );
				}
			}	
		}
		var callmes = new List();
		callmes.add( onComplete );
		loadQueue.push( { fileName: name, type: type, content: null, params: resParams, method: URLRequestMethod.HEAD, onCompleteCallBack: callmes } );
	}
	
	public function create( type : ResourceType, name : String, ?onComplete : CompletionStatus -> Dynamic -> Void ) {
		if ( onComplete == null ) {
			onComplete = defaultCompletionHandler;
		}
		var callmes = new List();
		callmes.add( onComplete );
		loadQueue.push( { fileName : name, type : type, content : null, params : null, method: URLRequestMethod.PUT, onCompleteCallBack : callmes } );
	}
	
	public function update( type : ResourceType, name : String, _content : Dynamic, ?onComplete : CompletionStatus -> Dynamic -> Void ) {
		if ( onComplete == null ) {
			onComplete = defaultCompletionHandler;
		}
		
		var callmes = new List();
		callmes.add( onComplete );
		loadQueue.push( { fileName : name, type : type, content : _content, params : null, method: URLRequestMethod.PUT, onCompleteCallBack : callmes } );
	}
	
	// save the specified fileName of the given type, optional onComplete handler will be called
	// onComplete is passed the status and the original content back
	public function save( type : ResourceType, name : String, _content : Dynamic, ?onComplete : CompletionStatus -> Dynamic -> Void ) {
		var callmes = new List();
		callmes.add( onComplete );
		loadQueue.push( { fileName : name, type : type, content : _content, params : null, method: URLRequestMethod.PUT, onCompleteCallBack : callmes } );
	}
	

	private static var theLoader;
	private var loadQueue : List<ResourceItem>;
	private var loadingHash : Hash<ResourceItem>;
	private var loadedHash : Hash<ResourceItem>;
	private var constantParams : URLVariables;

	private function tickLoadQueue() : Bool {
		// start loading each item in the load queue
		for ( i in loadQueue ) {
//			if( StringTools.startsWith( i.fileName, "http" ) ){
//				trace( "filename: " + i.fileName);
//			}
			if ( loadingHash.exists(i.fileName) ) {
				var qi : ResourceItem = loadingHash.get(i.fileName);
				for( callme in i.onCompleteCallBack ) {
					qi.onCompleteCallBack.add( callme );
				}
				loadQueue.remove(i);
				continue;
			}
			
			// if loaded, skip just send completation
			if ( loadedHash.exists(i.fileName) ) {
				for( callme in i.onCompleteCallBack ) {
					callme( CompletionStatus.Loaded, i.content );
				}
				loadQueue.remove(i);
				continue;
			}

			platformLoad( i );
			
			loadQueue.remove( i );
		}
		return true;
	}
	
	private function successHandler( me : ResourceLoader, req:URLRequest ) : Event -> Void {
		return function(e:Event) : Void {
			var item : ResourceItem = me.loadingHash.get(req.url);
#if cpp
			if ( Std.is( e.target, nme.display.LoaderInfo ) ) {
				var linfo = cast( e.target, nme.display.LoaderInfo );
				item.content = linfo.loader.content;
			} else			
#end
			{
				item.content = (Reflect.hasField(e.target, "content")) ? e.target.content : e.target.data;
			}
			me.loadingHash.remove(req.url);
			if( item.method == URLRequestMethod.GET ) {
				me.loadedHash.set( item.fileName, item);
			}
			for ( callme in item.onCompleteCallBack ) {
				callme( CompletionStatus.Loaded, item.content );
			}
		}
	}

	private function failHandler( failz : String, me : ResourceLoader, req:URLRequest ) : Event -> Void {
		return function(event:Event) : Void {
//			trace( req.url + " failed to load due to " + failz );
			var item : ResourceItem = me.loadingHash.get(req.url);
			me.loadingHash.remove(req.url);
			for( callme in item.onCompleteCallBack ) {
				callme( CompletionStatus.Failed, null );
			}
		}
	}
	private function logHandler( ev : String, me : ResourceLoader, req:URLRequest ) : Event -> Void {
		return function(event:Event) : Void {
			trace( ev + " fired for " + req.url );
			trace( event.target );
		}
	}
	
	private function platformLoad( i : ResourceItem ):Void {
		var loader : Dynamic;
		var url:String = i.fileName;

		if( StringTools.startsWith( url, "http" ) ){
			url += "?" + constantParams + ((i.params != null) ? "&" + i.params : "");
		}
		
		var request:URLRequest = new URLRequest( url );
		// flash only supports GET and POST, so we use the
		// X-HTTP-Method-Override header defacto standard to 
		// support the others. Many RESTful API support it
		// including our servers, Google APIs etc.
		if ( i.method != URLRequestMethod.GET ) {
			if( i.method != URLRequestMethod.POST ) {
				request.requestHeaders.push( new URLRequestHeader( "X-HTTP-Method-Override", i.method) );
			}
			request.method = URLRequestMethod.POST;
			request.data = (i.content == null) ? "OT" : i.content; // flash changes POST without payload to GET, so add FL payload
		} else {
			request.method = URLRequestMethod.GET;
		}

		switch( i.type ) {
			case RawBinary:
				loader = new URLLoader();
				loader.dataFormat = URLLoaderDataFormat.BINARY;
				loader.addEventListener( Event.CANCEL, logHandler( "Cancel", this, request ), false, 0, false );
				loader.addEventListener( Event.COMPLETE, successHandler( this, request ), false, 0, false );
				loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, failHandler( "Security", this, request), false, 0, false );
				loader.addEventListener( IOErrorEvent.IO_ERROR, failHandler( "IoError", this, request ), false, 0, false );
			
			case RawText:
				loader = new URLLoader();
				loader.dataFormat = URLLoaderDataFormat.TEXT;
				loader.addEventListener( Event.CANCEL, logHandler( "Cancel", this, request ), false, 0, false );
				loader.addEventListener( Event.COMPLETE, successHandler( this, request ), false, 0, false );
				loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, failHandler( "Security", this, request), false, 0, false );
				loader.addEventListener( IOErrorEvent.IO_ERROR, failHandler( "IoError", this, request ), false, 0, false );
			case Image:
				loader = new flash.display.Loader();
				loader.contentLoaderInfo.addEventListener( Event.CANCEL, logHandler( "Cancel", this, request ), false, 0, false );
				loader.contentLoaderInfo.addEventListener( Event.COMPLETE, successHandler( this, request ), false, 0, false );
				loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, failHandler( "Security", this, request), false, 0, false );
				loader.addEventListener( IOErrorEvent.IO_ERROR, failHandler( "IoError", this, request ), false, 0, false );

			case UrlParameters:
				loader = new URLLoader();
				loader.dataFormat = URLLoaderDataFormat.VARIABLES;
				loader.addEventListener( Event.CANCEL, logHandler( "Cancel", this, request ), false, 0, false );
				loader.addEventListener( Event.COMPLETE, successHandler( this, request ), false, 0, false );
				loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, failHandler( "Security", this, request), false, 0, false );
				loader.addEventListener( IOErrorEvent.IO_ERROR, failHandler( "IoError", this, request ), false, 0, false );

			default:
//				trace( "Internal load error of " + i.fileName + " Type : " + i.type );
				return;
		}

		loadingHash.set( url, i );
		try {
			loader.load( request );
		} catch( e : Dynamic ) {
//			trace( "File loading failed : " + url + " : " + e );
			for( callme in i.onCompleteCallBack ) {
				callme( CompletionStatus.Failed, null );
			}
			loadingHash.remove(url);
		}
	}

	
	private function new() {
		loadQueue = new List();
		loadingHash = new Hash();
		loadedHash = new Hash();
		
		var me = this;
		Exec.get().addJob( function(_) { return me.tickLoadQueue(); } );
	}
	
	private static function defaultCompletionHandler( status : CompletionStatus, content : Dynamic ) {
		switch( status ) {
		case Failed: 	// silently ignore failures
		case Loaded: 	// is now loaded but nothing to do post-load
		}
	}
}
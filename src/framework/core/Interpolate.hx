package framework.core;
import framework.core.Point;

enum InterpolationType {
	Linear;
}

/**
 * ...
 * @author Deano Calver
 */

class Interpolate {

	public static function Float( iType : InterpolationType,
									from : Float, to : Float, frames : Float,
									onUpdate : Float -> Bool,
									?onComplete : Void -> Bool ) {
		switch(iType ) {
			case Linear: {               
				var delta : Float = (to - from) / frames;
				var counter = frames;
				Exec.get().addJob( function( Void ) : Bool {
					from += delta;
					counter--;
					onUpdate( from );
					if ( counter > 0 ) {
						return true;
					} else {
						if (onComplete != null) onComplete();
						return false;
					}
				}, null );
			}
		}
	}
	
	public static function Point( iType : InterpolationType,
									from : Point, to : Point, frames : Float,
									onUpdate : Point -> Bool,
									?onComplete : Void -> Bool ) {
		switch(iType ) {
			case Linear: {
				var curX : Float = from.x;
				var curY : Float = from.y;
				var deltaX : Float = (to.x - curX) / frames;
				var deltaY : Float = (to.y - curY) / frames;
				var counter = frames;

				Exec.get().addJob( function( Void ) : Bool {
					curX += deltaX;
					curY += deltaY;
					onUpdate( new Point( curX, curY ) );
					counter--;
					if ( counter > 0 ) {
						return true;
					} else {
						if (onComplete != null) onComplete();
						return false;
					}
				}, null );
			}
		}
	}
	
}
package framework.net;
import framework.js.Node;
import framework.utils.DeepCopy;
import framework.utils.UnitTester;

/**
 * ...
 * @author Deano Calver
 */

class RouterTests {
	public function new() {
	}
	
	// mocks
	private static var mockResponse = {
		statusCode : 0,
		writeContinue: null,
		writeHead: null,
		setHeader: null,
		getHeader: null,
		removeHeader: null,
		end: null,
		addTrailers: null,
		write: null,		
	};
	private static var mockRequest = {
		method: "GET",
		url: "/vlash",
		headers: { host:"example.com" },
		trailers: "",
		httpVersion: "1.1",
		connection:null,
		setEncoding:null,
		pause: null,
		resume: null,		  
	};
	
	private var router0 : Router;
	private function setupUnitTests() {
		router0 = new Router( {
			dev: {
				log: {
					_script: function( resource : UriResource ) { 
						UnitTester.passOrFailz( resource.path== "/dev/" && resource.uriPostPath.length == 0 );
					},
					_exact: true,
				},
				_path: "dev"
			},
			
			_default: {
				_redirect: "/index.js",
			},
			// should be ignored
			__: {
			}
		});		
	}
	private function tearDownUnitTests() {
		router0 = null;
	}
	
	private function testDev() {	
		UnitTester.passOrFailz( router0.routeTable.nodes.exists("dev") );
	}
	private function testDevExact() {	
		UnitTester.passOrFailz( router0.routeTable.nodes.get("dev").exact == false );
	}
	private function test__() {	
		UnitTester.passOrFailz( router0.routeTable.nodes.exists("__") == false );
	}
	private function testDevLog() {	
		var dev = router0.routeTable.nodes.get( "dev" );
		UnitTester.passOrFailz( dev.nodes.exists("log") );
	}	
	private function testDevLogExact() {	
		var dev = router0.routeTable.nodes.get( "dev" );
		UnitTester.passOrFailz( dev.nodes.get("log").exact == true );
	}	
	private function testDefault_() {	
		UnitTester.passOrFailz( router0.routeTable.nodes.exists("_default") );
	}
	private function testDevLogRoute() {
		// mocks
		var mockReq = mockRequest;
		mockReq.url = "/dev/log";
		
		router0.route( mockReq, mockResponse );
	}
	private function testDefaultRoute() {

		var mockReq = mockRequest;
		var mockRes = mockResponse;
		
		mockRes.writeHead = function( statusCode : Int, ?reasonPhrase : String, headers : Dynamic ) {
				UnitTester.passOrFailz( headers.location == "http://example.com/index.js" );
			};
		mockRes.end = function( ?data : Dynamic, ?enc : String ) {}
		
		router0.route( mockReq, mockRes );
	}
	
}
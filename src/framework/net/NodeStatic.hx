package framework.net;

import haxe.Public;
import framework.net.Http;
import framework.js.Node;

/**
 * ...
 * @author Deano Calver
 */

extern class NodeStatic implements Public {
	function new( root : String, options : Dynamic ) : Void;

	function serve( request : HttpRequest, response : HttpResponse, ?callbak : String -> HttpResponse -> Void ) : Dynamic;

	public static function __init__() : Void {
		var ns = Node.require( "node-static" );
		untyped NodeStatic = ns.Server;
	}	
}

class NodeStaticWrapper {
	static public function wrap( instance : NodeStatic ) : UriResource -> Void {
		return function( resource : UriResource ) {
			switch( resource.method.toUpperCase() ) {
				case "GET":
					instance.serve( resource.request, resource.response );
				default:
					resource.response.writeHead( HttpReturnCodes.METHOD_NOT_ALLOWED, 
											resource.response );
					resource.response.end("");
			}
		}
	}	
	
}
package framework.net;

import haxe.Log;
import haxe.PosInfos;
import framework.utils.DeepCopy;
import framework.utils.UnitTester;

/**
 * ...
 * @author Deano Calver
 */

private class BasicCrudTest implements UriCRUD {
	
	public var pos : PosInfos;
	
	public function new ( ? _pos : PosInfos ) {		
		pos = _pos;
	}

	public function create( _resource : UriResource ) {		
		UnitTester.passOrFailz( _resource.method.toUpperCase() == "PUT", pos );
	}

	public function retrieve( _resource : UriResource ) {
		UnitTester.passOrFailz( _resource.method.toUpperCase() == "GET", pos );
	}

	public function update( _resource : UriResource ) {
		UnitTester.passOrFailz( _resource.method.toUpperCase() == "POST", pos );
	}
	
	public function destroy( _resource : UriResource ) {
		UnitTester.passOrFailz( _resource.method.toUpperCase() == "DELETE", pos );
	}
}

// the basic test purely test the routing and delegation to the UriREST interface
// is operating correctly, doesn't test response or actions are coorect
private class BasicRestTest implements UriREST {

	public var pos : PosInfos;
	
	public function new ( ? _pos : PosInfos ) {		
		pos = _pos;
	}

	public function index( _resource : UriResource ) {
		UnitTester.passOrFailz( 
			_resource.method.toUpperCase() == "GET" &&
			_resource.path == "/test1/", pos );
	}

	public function createupdate( _resource : UriResource ) {
		UnitTester.passOrFailz(
		_resource.method.toUpperCase() == "PUT" &&
		_resource.path == "/test1/" , pos );
	}
	
	public function retrieve( _resource : UriResource ) {
		UnitTester.passOrFailz( 
			_resource.method.toUpperCase() == "GET" &&
			_resource.path == "/test1/" &&
			_resource.uriPostPath[0] == "0", pos );
	}
	
	public function destroy( _resource : UriResource ) {
		UnitTester.passOrFailz( 
			_resource.method.toUpperCase() == "DELETE" &&
			_resource.path == "/test1/" &&
			_resource.uriPostPath[0] == "2", pos );
	}
	
	public function precreate( _resource : UriResource ) {
		UnitTester.passOrFailz( 
			_resource.method.toUpperCase() == "HEAD" &&
			_resource.path == "/test1/" &&
			_resource.queryParams[0] == "CLIENT_ID=0" &&
			_resource.queryParams[1] == "CUID=1" &&
			_resource.uriPostPath.length == 0, pos );
	}
	
	public function metadata( _resource : UriResource ) {
		UnitTester.passOrFailz( 
			_resource.method.toUpperCase() == "HEAD" &&
			_resource.path == "/test1/" &&
			_resource.uriPostPath[0] == "0", pos );
	}
}

class RestfulTests {
	private var router0 : Router;
	private var crud0 : BasicCrudTest;
	private var rest0 : BasicRestTest;

	// mocks
	private static var mockRequest = {
		method: "POST",
		url: "/test0",
		headers: { host:"example.com" },
		trailers: "",
		httpVersion: "1.1",
		connection:null,
		setEncoding:null,
		pause: null,
		resume: null,		
	};
	
	private static var mockResponse = {
		statusCode : 0,
		writeContinue: null,
		writeHead: null,
		setHeader: null,
		getHeader: null,
		removeHeader: null,
		end: null,
		addTrailers: null,
		write: null,		
	};
		
	public function new() {		
	}

	private function setupUnitTests() {
		crud0 = new BasicCrudTest();
		rest0 = new BasicRestTest();
		
		var me = this;
		
		mockResponse.writeHead = 
			function( statusCode:Int, ?reasonPhrase:String, headers:Dynamic) {
				UnitTester.failz( me.rest0.pos );
			}
		mockResponse.end = 
			function(?data:Dynamic, ?enc:String) {
			}		
		
		router0 = new Router( {
			test0: {
				_path: "test0",
				_script: RESTful.crudWrapper( crud0 ),
			},
			test1: {
				_path: "test1",
				_script: RESTful.restfulWrapper( rest0 ),				
			}
		});
	}
	
	private function tearDownUnitTests() {
		router0 = null;
		crud0 = null;
		rest0 = null;
	}
	
	private function testCrudCreate() {
		mockRequest.method = "PUT";		
		router0.route( mockRequest, mockResponse );
	}

	private function testCrudRetrieve() {
		mockRequest.method = "GET";
		router0.route( mockRequest, mockResponse );
	}

	private function testCrudUpdate() {
		mockRequest.method = "POST";
		router0.route( mockRequest, mockResponse );
	}
	
	private function testCrudDelete() {
		mockRequest.method = "DELETE";
		router0.route( mockRequest, mockResponse );
	}
	
	private function testRestfulIndex() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "GET";
		mockRequest.url = "/test1";

		router0.route( mockRequest, mockResponse );
	}

	private function testRestfulPreCreate() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "HEAD";
		mockRequest.url = "/test1?CLIENT_ID=0&CUID=1";

		router0.route( mockRequest, mockResponse );
	}
	
	private function testRestfulCreate() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "PUT";
		mockRequest.url = "/test1/blash?CLIENT_ID=0";

		router0.route( mockRequest, mockResponse );
	}

	private function testRestfulGet() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "GET";
		mockRequest.url = "/test1/0";

		router0.route( mockRequest, mockResponse );
	}	
	
	private function testRestfulMetadata() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "HEAD";
		mockRequest.url = "/test1/0";

		router0.route( mockRequest, mockResponse );
	}	
	
	private function testRestfulUpdate() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "PUT";
		mockRequest.url = "/test1/1";

		router0.route( mockRequest, mockResponse );
	}	
	
	private function testRestfulDelete() {

		rest0.pos = (function(?p:haxe.PosInfos) return p)();
		mockRequest.method = "DELETE";
		mockRequest.url = "/test1/2";

		router0.route( mockRequest, mockResponse );
	}	
	
}
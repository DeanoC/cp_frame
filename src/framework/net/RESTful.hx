package framework.net;

import framework.net.UriCRUD;
import framework.net.UriREST;
import framework.net.Http;
/**
 * ...
 * @author Deano Calver
 */
class RESTful {
	
	// NOTE this wrapper is provided as a test / conveience it is 
	// 		NOT complient with our cloud API rules and as such should
	// 		NOT be used for implementing our APIs
	public static function crudWrapper( instance : UriCRUD ) {
		// map HTTP 1.0/1 methods to CRUD semantics
		return function( resource : UriResource ) {
			var method = resource.method.toUpperCase();
			if ( method == "POST" ) {
				var ov = Reflect.field( resource.request.headers, "X-HTTP-Method-Override" );
				if ( ov != null ) {
					method = ov;
					resource.httpMethodOverride = true;
				}
			}
			
			switch( method ) {
				case "GET":
					instance.retrieve( resource );
				case "POST":
					instance.update( resource );
				case "PUT":
					instance.create( resource );
				case "DELETE":
					instance.destroy( resource );
				default:
					resource.response.writeHead( HttpReturnCodes.METHOD_NOT_ALLOWED, 
											resource.response );
					resource.response.end("");
			}
		}
	}
	
	public static function restfulWrapper( instance : UriREST ) {
		// RESTful semantics mapper
		// GET uri 		=> index all resources
		// PUT uri 		=> create (see docs)
		// HEAD uri		=> precreate (see docs)
		// --CRUD-like resource operations--
		// PUT uri/0 	=> create/update resource 0
		// GET uri/0 	=> retrieve resource 0
		// DELETE uri/0 => destroy resource 0
		// HEAD uri/0  	=> metadata resource 0
		// ---- Extension ----
		// Many system only allow GET and POST (*cough* Flash!)
		// So a defecto standard has emerged that encodes the others
		// in a X-HTTP-Method-Override header in a POST, we support
		// that here
		return function( resource : UriResource ) {
			var method = resource.method.toUpperCase();
//			trace( method );
//			trace( resource.request.headers );
			if ( method == "POST" ) {
				var ov = Reflect.field( resource.request.headers, "x-http-method-override" );
				if ( ov != null ) {
					method = ov;
					resource.httpMethodOverride = true;
				}
			}
			switch( method ) {
				case "GET":
					if ( resource.uriPostPath.length == 0 ) {
						instance.index( resource );
					} else {
						instance.retrieve( resource );
					}
				case "HEAD":
					if ( resource.uriPostPath.length == 0 ) {
						instance.precreate( resource );
					} else {
						instance.metadata( resource );
					}
				case "PUT":
					if ( resource.uriPostPath.length == 0 ) {
						resource.response.writeHead( HttpReturnCodes.METHOD_NOT_ALLOWED, "METHOD_NOT_ALLOWED", {}  );
						resource.response.end("");
					} else {
						instance.createupdate( resource );
					}
				case "DELETE":
					if ( resource.uriPostPath.length == 0 ) {
						resource.response.writeHead( HttpReturnCodes.METHOD_NOT_ALLOWED,"METHOD_NOT_ALLOWED", {});
						resource.response.end("");
					} else {
						instance.destroy( resource );
					}
					// TODO investigate further. Browser XHR is sending options first...
				case "OPTIONS":
					var headers = {};
					Reflect.setField( headers, "Access-Control-Allow-Origin", "*" );
					Reflect.setField( headers, "Access-Control-Allow-Methods", "GET,HEAD,PUT,DELETE,OPTIONS,POST" );
					resource.response.writeHead( HttpReturnCodes.OK, "OK", headers );
//					trace( "OPTIONS response headers" + headers );
					resource.response.end("");					
				default:
					resource.response.writeHead( HttpReturnCodes.METHOD_NOT_ALLOWED, 
											"METHOD_NOT_ALLOWED", {} );
					resource.response.end("");
					
			}
		}
	}
	
}
package framework.net;

import framework.net.UriResource;
/**
 * ...
 * @author Deano Calver
 */

interface UriCRUD {
	function create( _resource : UriResource ) : Void;
	function retrieve( _resource : UriResource ) : Void;
	function update( _resource : UriResource ) : Void;
	function destroy( _resource : UriResource ) : Void;
}
package framework.net;

import framework.net.Http;

/**
 * ...
 * @author Deano Calver
 */

typedef UriResource = {	
	var method: String;
	var	path: String;
	var uriPostPath: Array<String>;
	var queryParams: Array<String>;
	var searchPart: String;
	var request : HttpRequest;
	var response : HttpResponse;
	var httpMethodOverride : Bool;
}
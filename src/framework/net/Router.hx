package framework.net;
import haxe.Public;
import framework.net.Http;

/**
 * Router is an important part of a server side environment, all access to the server should be pushed
 * here, the job of this script is to convert the url passed in, into the correct destination and
 * then include and run that particular script
 * @author Deano Calver
 * 
 * See cloudnode documentation for details
**/
class Router {
	public var routeTable(default,null) : RouteNode;	

	public function new( rtTable : Dynamic ) {
		var stk = new Array<RouteNode>();	
		routeTable = new RouteNode(rtTable);
		stk.push( routeTable );
		
		while ( stk.length > 0 ) {
			var curNode = stk.pop(); 
			var fields = Reflect.fields( curNode.orig );			
			for ( entry in fields ) {
				var field = Reflect.field( curNode.orig, entry );
				if ( StringTools.startsWith( entry, "_" ) == true && 
												entry != "_default" ) {
													
					switch( entry ) {
						case "_path":
							curNode.path = field;
						case "_script":
							curNode.script = untyped field;
						case "_exact":
							curNode.exact = cast( field, Bool);
						case "_redirect":
							curNode.redirect = field;
					}
				} else {
					var childNode = new RouteNode( field );
					curNode.nodes.set( entry, childNode );
					stk.push( childNode );
				}
			}
			curNode.orig = null;
		}
	}

	public function route( request : HttpRequest, response : HttpResponse ) {
		var urlSplit = request.url.split('?');
		var pathFromClient = urlSplit[0];
		var path = pathFromClient.split('/');
		var params = urlSplit[1];

		// first entry is almost certainly just a "" due to / on it own, shift it out of existence
		if (path[0] == "") {
			path.shift();
		}

		router( path, params, request, response );
	}

	//-------------------------------------
	private function router( path : Array<String>, params : String, _request : HttpRequest, _response : HttpResponse ) {
		var postScriptPath = [];
		var script = null;
		var routePath = "/";
		try {
			var route = routeTable;
			var subScript;
			for ( i in 0 ... path.length ) {
				if ( route.nodes.exists( path[i] ) == true ) {
					route = route.nodes.get( path[i] );
					if( route.exact == true ) {
						if(path.length > i+1) {
							// any more can't be an exact match so we are done mapping
							break;
						}
					}
					if ( route.path != null ) {
						routePath = routePath + route.path + "/";
					}

					if ( route.script != null ) {
						Debug.assert( Reflect.isFunction( route.script ) == true );
						script = route.script;

						//produce a list of paths post the script for it to process if it wants
						postScriptPath = [];
						var j = i + 1;
						var k = 0;
						while( j < path.length ) {
							postScriptPath[k] = path[j];
							++j;
							k++;
						}
					}
				}
			}

			if (route.script == null) {
				if ( routeTable.nodes.exists("_default") ) {
					var def = routeTable.nodes.get("_default");
					if( def.redirect != null ) {
						var redTo;
						if( def.redirect[0] == '/') {
							redTo = 'http://' + _request.headers.host + def.redirect;
						} else {
							redTo = def.redirect;
						}
						_response.writeHead(HttpReturnCodes.TEMP_REDIRECT, "", { location: redTo } );
						_response.end('');
						return;
					} else if( def.script != null ) {
						if ( def.path != null) {
							routePath = routePath + route.path + '/';
						}
						script = def.script;
					} else {
						// final fall back
						_response.writeHead(HttpReturnCodes.TEMP_REDIRECT, "", {location: 'http://' + _request.headers.host + '/index.html'});
						_response.end('');
						return;
					}
				}
			}
		} catch ( e : Dynamic ) {
			_response.writeHead( HttpReturnCodes.INTERNAL_SERVER_ERROR, {location:"/"} );
			_response.end("Sorry, we have suffered a routing failure, I blame evil gremlins for it!");
			return;
		}

		if ( script != null ) {
			var sqp = if ( params != null) params.split('#') else ["",""];
			var qp = if ( sqp[0].length > 0 ) sqp[0].split('&') else [""];
			var sp = sqp[1];
			
			var resource = {
				method: _request.method,
				path: routePath,
				uriPostPath: postScriptPath,
				queryParams: qp,
				searchPart: sp,
				response : _response,
				request : _request,
				httpMethodOverride : false,
			};
							
			script( resource );
		} else {
			Debug.assert( script != null, _request.url );
		}
	}
}

private class RouteNode implements Public {
	var nodes: Hash<Dynamic>;
	var orig: Dynamic;
	
	function new ( _orig : Dynamic ) {
		nodes = new Hash<Dynamic>();
		orig = _orig;
		path = null;
		exact = false;
		script = null;
		redirect = null;
	}
	
	// optional
	var path : String;
	var exact : Bool;
	var script : UriResource -> Void;
	var redirect: String;
}

package framework.net;
import framework.js.Node;

/**
 * ...
 * @author Deano Calver
 */

typedef HttpRequest = NodeHttpServerReq;
typedef HttpResponse = NodeHttpServerResp;

// prettier http return codes
class HttpReturnCodes {
	public static var CONTINUE = 100;
	public static var SWITCH_PROTOCOL = 101;
	public static var PROCESSING = 102;
	public static var NON_STD_URI_TOO_LONG = 122;
	
	public static var OK = 200;
	public static var CREATED = 201;
	public static var ACCEPTED = 202;
	public static var NON_AUTH_INFO = 203;
	public static var NO_CONTENT = 204;
	public static var RESET_CONTENT = 205;
	public static var PARTIAL_CONTENT = 206;
	public static var MULTI_STATUS = 207;
	public static var IM_USED = 226;
	
	public static var MULTIPLE_CHOICES = 300;
	public static var MOVED_PERMANENTLY = 301;
	public static var FOUND = 302;
	public static var SEE_OTHER = 303;
	public static var NOT_MODIFIED = 304;
	public static var USE_PROXY = 305;
	public static var SWITCH_PROXY = 306;
	public static var TEMP_REDIRECT = 307;
	
	public static var BAD_REQUEST = 400;
	public static var UNAUTHORIZED = 401;
	public static var PAYMENT_REQUIRED = 402;
	public static var FORBIDDEN = 403;
	public static var NOT_FOUND = 404;
	public static var METHOD_NOT_ALLOWED = 405;
	public static var NOT_ACCEPTABLE = 406;
	public static var PROXY_AUTH_REQUIRED = 407;
	public static var REQUEST_TIMEOUT = 408;
	public static var CONFLICT = 409;
	public static var GONE = 410;
	public static var LENGTH_REQUIRED = 411;
	public static var PRECONDITION_FAILED = 412;
	public static var ENTITY_TOO_LARGE = 413;
	public static var URI_TOO_LONG = 414;
	public static var UNSUPPORTED_MEDIA_TYPE = 415;
	public static var REQUESTED_RANGE_NOT_SATISFIABLE = 416;
	public static var EXPECTATION_FAILED = 417;
	public static var IM_A_TEAPOT = 418;
	public static var UNPROCESSABLE_ENTITY = 422;
	public static var LOCKED = 423;
	public static var FAILED_DEPENDENCY = 424;
	public static var UNORDERED_COLLECTION = 425;
	public static var UPGRADE_REQUIRED = 426;
	public static var NO_RESPONSE = 444;
	public static var RETRY_WITH = 449;
	public static var BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS = 450;
	public static var CLIENT_CLOSED_REQUEST = 499;
	
	public static var INTERNAL_SERVER_ERROR = 500;
	public static var NOT_IMPLEMENTED = 501;
	public static var BAD_GATEWAY = 502;
	public static var SERVICE_UNAVAILABLE = 503;
	public static var GATEWAY_TIMEOUT = 504;
	public static var HTTP_VERSION_NOT_SUPPORTED = 505;
	public static var VARIANT_ALSO_NEGOTIATES = 506;
	public static var INSUFFICIENT_STORAGE = 507;
	public static var NON_STD_BANDWIDTH_LIMIT_EXCEEDED = 509;
	public static var NOT_EXTENDED = 510;
	
}
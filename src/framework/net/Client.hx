package framework.net;
import framework.core.Exec;
import flash.display.DisplayObject;
import framework.utils.Config;

import net.Facebook;

import core.ResourceLoader;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import framework.utils.Json;

/**
 * ...
 * @author Deano Calver
 */

class Client {
	public var loggedIn(default, null) : Bool;
	public var clientDetails(default, null) : Dynamic;
	
	public function new( serverBaseURL : String ) {
		serverLoc = serverBaseURL;

		gameId = "";
		loggedIn = false;
	}
	
	/** when the net is up will call handler(true),
	 * if not will call handler(false) and try again in a short while.
	 * in normal usage you can checkNetUp and wait for handler(true) to be called
	 * eventually, use the handler(false) to display something nice to the user
	 */
	public function checkNetUp( handler : Bool -> Void ) {
		var me = this;
		var completionHandler= function ( status : CompletionStatus, content : Dynamic ) {
			switch( status ) {
				case Failed:
					handler(false);
					Exec.get().addDelayedJob( 990, function(Void) { me.checkNetUp(handler); return false;  } );
				case Loaded: handler(true);
			}
		}

		ResourceLoader.get().load( RawText, serverLoc + '/crossdomain.xml', completionHandler );
	}
	
	public function login() {
		var dialog = new display.dialog.LoginDialog();
		dialog.display();
	}
	
	public function createGame( gameName : String, handler : Bool -> Void ) {
		var me = this;
		
		var clsid = Std.random( 0xFFFF );

		ResourceLoader.get().head( RawText, serverLoc + "/game/" + gameName, ["clsid=" + clsid], 
			function ( status : CompletionStatus, content : Dynamic ) {
				switch( status ) {
					case Loaded:
						var dec = Json.decode( content );
						var uri = me.serverLoc + dec.uri + "/create";
						
						ResourceLoader.get().create( RawText, uri,
							function ( status : CompletionStatus, content : Dynamic ) {
								if ( status == Loaded ) {
									me.gameId = dec.uri;
									handler( true );
								} else {
									handler( false );
								}
							}
						);
					case Failed:
						// TODO retry + handle things better
						handler( false );
				}
		});
	}
	
	public function joinGame( gameName : String, handler : Bool -> Void ) {
		var me = this;
		ResourceLoader.get().load( RawText, serverLoc + "/game/" + gameName, ["count=1"],
			function( status : CompletionStatus, content : Dynamic ) {
				if ( status == Loaded ) {
					var gameInfo = Json.decode(content);
					me.gameId = gameInfo.uri;
					handler( true );
				} else {
					// no games! :( 
					handler( false );
				}
			}
		);
	}
	
	// handler will be called with the haxe serilized data or null
	public function getInitialGame( handler : Dynamic -> Void ) {
		var uri = serverLoc + gameId + "/all";
		ResourceLoader.get().load( RawText, uri, 
			function( status : CompletionStatus, content : Dynamic ) {
				if ( status == Loaded && content != null && content != "" ) {
					var submit = Json.decode(content);
					handler( submit.log );
				} else {
					handler(null);
				}
			}
		);
	}
	
	public function sendLocalUpdate( data : Dynamic ) {
		var turn = data.turn;
		var jsonData = Json.encode( data );

		var me = this;
		var turnUri = serverLoc + gameId + "/turn/" + turn + "/" + Config.config.userId;		
		// check if we have submitted our turn data and the server has it
		ResourceLoader.get().head( RawText, turnUri, [], 
			function ( status : CompletionStatus, content : Dynamic ) {
				if ( status != Loaded ) {
					ResourceLoader.get().update( RawText, turnUri, jsonData,
						function ( status : CompletionStatus, content : Dynamic ) {
							// whatever the reply, ensure its on the server by
							// recalling this function
							me.sendLocalUpdate( data );
						}
					);
				} else {
					// this means we recieved an OK, so the server already
					// has this update from us. This should be good (unless there
					// is a client bug and we have send two different turn datas
					// in the same turn, which is tough for us. Server has got 
					// something from us and thats it.
					return;
				}
			}
		);
	}
	
	public function isServerReady( turn : Int, handler : Bool -> Void ) {
		var turnUri = serverLoc + gameId + "/ready/" + turn;
		
		ResourceLoader.get().head( RawText, turnUri, [], 
			function ( status : CompletionStatus, content : Dynamic ) {
				handler( status == Loaded );
			}
		);
	}
	
	public function updateGame(turn : Int, handler : Dynamic -> Void ) {
		var turnUri = serverLoc + gameId + "/turn/" + turn;
		
		ResourceLoader.get().load( RawText, turnUri,
			function ( status : CompletionStatus, content : String ) {
				if( status == Loaded ) {
					var submit = Json.decode(content);
					handler( submit.log );
				} else {
					handler( null );
				}
			}
		);
	}
	
	
/*			
	public function loginViaFacebook(clientId : String) {
		#if !cpp
		// required extended permissions
		var scope = "publish_stream,user_photos,user_photo_video_tags";

		var facebook = new Facebook();
		var url = serverLoc + '/login?client_id=' + clientId + '&session_guid=' + sessionGuid;
		var me = this;
		facebook.connect(url);
		dj = function(Void) : Bool {
			if( me.loggedIn == false  ) {
				ResourceLoader.get().load( RawText, me.serverLoc + '/login/session?session_guid=' + me.sessionGuid,
					function( status : CompletionStatus, content : Dynamic ) {
					if( status == Loaded && content == 'OK' ) {
						me.loggedIn = true;
						trace('logged in');
						me.getClientDetails();
					}
				});
				Exec.get().addDelayedJob( 33, me.dj );
			}
			return false;
		}
		
		dj( Void );
		#end
	}

	public function cloudLoad( loadId : String, onComplete : String -> Void ) {
		var me = this;
		ResourceLoader.get().load( RawText, me.serverLoc + '/cloud/load?session_guid=' + me.sessionGuid + '&save_id=' + loadId,
			function( status : CompletionStatus, content : Dynamic ) {
			if ( status == Loaded ) {
				var str : String = content;
				if ( str.split(":")[0].indexOf("error") == -1 ) {
					onComplete( content );
				} else {
					onComplete(null);
				}
			}
		});
	}
	public function cloudSave( loadId : String, content : Dynamic, ?onComplete : Bool -> Void ) {
		var me = this;
		var send : String = 'session_guid=' + me.sessionGuid + '&save_id=' + loadId + '&data=' + content;
		ResourceLoader.get().save( RawText, me.serverLoc + '/cloud/save', send,
			function( status : CompletionStatus, Void ) {
				if ( status == Loaded ) {
//					(onComplete != null) ? onComplete( true ) : null;
				} else {
//					(onComplete != null) ? onComplete( false ) : null;
				}
		});
	}
	
	private function getClientDetails() {
		var me = this;
		ResourceLoader.get().load( RawText, me.serverLoc + '/social/me?session_guid=' + me.sessionGuid,
			function( status : CompletionStatus, content : Dynamic ) {
			if ( status == Loaded ) {
				me.clientDetails = Json.decode( content );
			}
		});
	}
		

	private function S4() {
		// std.string should really be a hex radix 16 converter but this does for what I need
  		var str : String = Std.string( (((1 + Std.int(Math.random() * 0x10000))) | 0) );
		return str.substr(1);
	}
	
	private function guid() {
		return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}

#if !cpp
	private var facebook : Facebook;
#end
*/
	private var serverLoc : String;
	private var gameId : String;

	private var dj : Dynamic -> Bool;

}
package framework.net;

/**
 * ...
 * @author Deano Calver
 */

interface UriREST {
	function index( _resource : UriResource ) : Void;
	function metadata( _resource : UriResource ) : Void;
	function precreate( _resource : UriResource ) : Void;

	function retrieve( _resource : UriResource ) : Void;
	function createupdate( _resource : UriResource ) : Void;
	function destroy( _resource : UriResource ) : Void;	
}
package framework.net;

#if !cpp
import flash.external.ExternalInterface;
/**
 * ...
 * @author Deano Calver
 */
class Facebook {

	public function new() {
	}

	public function connect(url) : Void {
        if (ExternalInterface.available) {
			var name : String = "facebookConnector";
			var props : String = "width=670,height=400";
			var js : String = ''
				+ 'var fbb = document.getElementById("fb_butt");'
				+ 'fbb.onclick = function() { window.open("' + url + '", "' + name + '", "' + props + '"); };'
				;
#if js
			ExternalInterface.addCallback( "fb_cb", "function(){" + js + "}" );
			ExternalInterface.call("fb_cb");
#else
			ExternalInterface.call("function(){" + js + "}");
#end
		} else {
			trace( "No External Interface means we can't open a browser page which is required by Facebook :(" );
		}
	}
}
#end
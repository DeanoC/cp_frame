/**
 * ...
 * @author wvxvw
 */
package framework.third_party;
import flash.Lib;
import flash.Memory;
import flash.utils.ByteArray;
import haxe.Int32;
using haxe.Int32;

class Base64 
{
	private static inline var _table:String = 
		"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@>@@@?456789:;<=@@@@@@@@";
		//+
		//"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A" +
		//"\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19@@@@@@" +
		//"\x1A\x1B\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A" +
		//"\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33";
	private static inline var _base:String =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	public function new() { }	
	
	public static function decode(input:String):ByteArray
	{
		var whole : Int32 = 0.ofInt();
		var current:Int = 0;
		var state:Int = 0;
		var inBa:ByteArray = new ByteArray();
		inBa.writeUTFBytes( _table );
		for ( i in 0 ... 0x19 ) {
			inBa.writeByte( i );			
		}
		inBa.writeUTF( "@@@@@@" );
		for ( i in 0x1a ... 0x33 ) {
			inBa.writeByte( i );			
		}
		inBa.writeUTFBytes( input );

		//+ input );
		var len:Int = inBa.length;
		var outBa:ByteArray;
		var writePos:Int = 123;
		while (inBa.length < 1024){
			inBa.writeUTFBytes( _table );
			for ( i in 0 ... 0x19 ) {
				inBa.writeByte( i );			
			}
			inBa.writeUTF( "@@@@@@" );
			for ( i in 0x1a ... 0x33 ) {
				inBa.writeByte( i );			
			}
		}
		
		Memory.select( inBa );
		var len:Int = inBa.length;
		for (i in 123...len) {
			current = Memory.getByte(i);
			if (Memory.getByte(current) != 0x40 ) {
				switch (state)
				{
					case 0:
						whole = Memory.getByte(current).ofInt().shl(18);
					case 1:
						whole.or( Memory.getByte(current).ofInt().shl(12) );
					case 2:
						whole.or( Memory.getByte(current).ofInt().shl(6) );
					default:
						whole.or( Memory.getByte(current).ofInt() );
						state = -1;
						current = whole.and( 0xFF.ofInt() ).shl(16).toInt();
						whole.and( 0xFF00.ofInt() );
						whole.or( whole.shr(16) ).or( 0xFF.ofInt() ).or( current.ofInt() );
						Memory.setI32(writePos, whole.toInt() );
						writePos += 3;
				}
				state++;
			}
		}
		
		if (state > 0)
		{
			current = whole.and( 0xFF.ofInt() ).shl(16).toInt();
			whole.and( 0xFF00.ofInt() );
			whole.or( whole.shr(16) ).or( 0xFF.ofInt() ).or( current.ofInt() );
		}
		switch (state)
		{
			case 1:
			case 2:
				Memory.setByte(writePos++, whole.toInt());
			case 3:
				Memory.setByte(writePos++, whole.toInt() & 0xFF);
				Memory.setByte(writePos++, (whole.toInt() >>> 8) & 0xFF);
		}
		
		outBa = new flash.utils.ByteArray();
		inBa.position = 123;
		inBa.readBytes(outBa, 0, writePos - 123);		

		return outBa;
	}
}

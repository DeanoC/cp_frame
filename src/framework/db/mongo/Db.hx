package framework.db.mongo;

import haxe.Public;
import framework.db.mongo.Mongodb;

/**
 * ...
 * @author Deano Calver
 */

extern class Db implements Public {

	function new( databaseName : String, serverConfig : Dynamic, ?options : Options ) : Void;

	function open( ?callbak : Callback ) : Void;
	function close() : Void;
	function admin( ?callbak : Callback ) : Void;

	function collectionsInfo( ?collectionName : String, callbak : Callback ) : Void;
	function collectionNames( ?collectionName : String, callbak  : Callback ) : Void;
	function collection( ?collectionName : String, ?options : Options, callbak : Callback ) : Void;
	function collections( callbak : Callback ) : Void;

	function eval( code : String, parameters : Array<String>, ?callbak : Callback ) : Void;

	function dereference( dbRef : String, callbak : Callback ) : Void;
	function logout( options : String, ?callbac : Callback ) : Void;
	function authenticate( userName : String, passWord : String, ?callbak : Callback ) : Void;
	function addUser( userName : String, passWord : String, ?callbak : Callback ) : Void;
	function removeUser( userName : String, ?callbak : Callback ) : Void;
	function createCollection( collectionName : String, ?options : Options, ?callbak : Callback ) : Void;
	function command( selector : String, ?callbak : Callback ) : Void;
	function dropCollection( collectionName : String ) : Void;
	function renameCollection( fromCollectionName : String, toCollectionName : String, ?callbak : Callback ) : Void;

	function lastError( ?options : String, callbak : Callback ) : Void;
	function error( ?options : String, callbak : Callback ) : Void;
	function lastStatus( callbak : Callback ) : Void;
	function previousErrors( callbak : Callback ) : Void;

	function executeDbCommand( commandHash : String, ?callbak : Callback ) : Void;
	function executeDbAdminCommand( commandHash : String, ?callbak : Callback ) : Void;

	function resetErrorHistory( ?callbak : Callback ) : Void;
	
	function createIndex( collectionName : String, ?fieldOrSpec : String, ?options : Options, ?callbak : Callback ) : Void;
	function ensureIndex( collectionName : String, ?fieldOrSpec : String, ?options : Options, ?callbak : Callback ) : Void;
	function cursorInfo( callbak : Callback ) : Void;
	function dropIndex( collectionName : String, indexName : String, ?callbak : Callback ) : Void;
	function indexInformation( collectionName : String, ?options : Options, ?callbak : Callback ) : Void;

	function dropDatabase( ?callbak : Callback ) : Void;

	function executeCommand( db_command : String, ?options :Options, ?callbak : Callback ) : Void;
	
	static var DEFAULT_URL : String;	
}

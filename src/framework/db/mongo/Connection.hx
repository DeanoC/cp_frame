package framework.db.mongo;

import haxe.Public;
import framework.js.Node;

/**
 * ...
 * @author Deano Calver
*/
extern class Connection implements Public {
	
	function new( host : String, port : Int, ?autoReconnect : Bool, ?options : String ) : Void;
	
	function open() : Void;
	
	function close() : Void;
	
	function send( command : Dynamic, rawConnection : Dynamic ) : Void;

	function sendWithoutReconnect( command : Dynamic ) : Void;
	
	static var DEFAULT_PORT : Int;
	
	function getNodeEventEmitterInterface() : NodeEventEmitter {
		return cast this;
	}
}

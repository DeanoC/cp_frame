package framework.db.mongo;

import framework.db.mongo.Mongodb;
import haxe.Public;

/**
 * ...
 * @author Deano Calver
 */

extern class Admin implements Public {
	function new( db : Db ) : Void;
	
	function serverInfo( callbak : Callback ) : Void;
	function profilingLevel( callbak : Callback ) : Void;
	function authenticate( username : String, password : String, callbak : Callback ) : Void;
	function logout( options : Options, callbak : Callback ) : Void;
	function addUser( username : String, password : String, callbak : Callback ) : Void;
	function setProfilingLevel( level : String, callbak : Callback ) : Void;
	function profilingInfo( callbak : Callback ) : Void;
	function command( command : String, callbak : Callback ) : Void;
	function validateCollection( collectionName : String, callbak : Callback ) : Void;	 
}
package framework.db.mongo.connections;
import framework.db.mongo.Mongodb;

/**
 * ...
 * @author Deano Calver
 */

extern class Server {

	function new( host : String, port : Int, options : Mongodb.Options ) : Void;
	
}
package framework.db.mongo;

/**
 * ...
 * @author Deano Calver
 */

extern class ObjectId {

	public function new( ?id : String ) : Void;
	
	public function get_inc() : Int;
	
	public function generate() : String;
	
	public function toHexString() : String;
	
	public function toString() : String;
	
}
package framework.db.mongo;

import framework.db.mongo.Mongodb;

/**
 * ...
 * @author Deano Calver
 */

typedef Collection = {
	// docs can either be a string, single JSON document or an array of strings
	function insert( docs : Dynamic, options : Options, ?callbak : Callback ) : Void;
	function checkCollectionName( collectionName : String ) : Void;
	function remove( selector : Dynamic, ?options : Options, ?callbak : Callback ) : Void;
	function rename( newName : String, callbak : Callback ) : Void;
	function insertAll( docs : Array< Dynamic >, ?options : Options, ?callbak : Callback ) : Void;
	function save( doc : Dynamic, ?options : Options, callbak : Callback ) : Void;
	function update( selector : Dynamic, doc : Dynamic, ?options : Options, ?callbak : Callback ) : Void;
	function distinct( key : String, ?query : Query, ?callbak : Callback ) : Void;
	function count( query : Query, callbak : Callback ) : Void;
	function drop( callbak : Callback ) : Void;
	function findAndModify( query : Query, sort : Array< String >, doc : String, options : Options, callbak : Callback ) : Void;
	// TODO find has lots of parameter variations...
	function find( selector : Dynamic, ?options : Options, ?callbak : Callback ) : Cursor;
	function normalizeHintField( hint : Dynamic ) : Dynamic;
	function findOne( queryObject : Query, options : Options, callbak : Callback ) : Void;
	function createIndex( ?fieldorSpec : Dynamic,  ?options : Options, ?callbak : Callback ) : Void;
	function indexInformation( ?options : Options, ?callbak : Callback ) : Void;
	function dropIndex( indexName : String, ?callbak : Callback ) : Void;
	function dropIndexes( ?callbak : Callback ) : Void;
	function mapReduce( map : Dynamic, reduce : Dynamic, options : Options, callbak : Callback ) : Void;
	function group( keys :Dynamic, condition : Dynamic, initial : Dynamic, reduce : Dynamic, command : Bool,  callbak : Callback ) : Void;
	function options( callbak : Callback ) : Void;	
}
package framework.db.mongo;
import framework.utils.UnitTester;
import framework.utils.Config;
/**
 * ...
 * @author Deano Calver
 */

class Tests {

	public function new() {
	}
	/*
	private function testConnect() {
		new Mongodb( "admin", function( m : Mongodb ) {
			UnitTester.success();
			m.finished();
		});		
	}
	
	
	private function testAdmin() {
		new Mongodb( "admin", function( m : Mongodb ) {
			var admin = new Admin( m.mdb );				
			admin.serverInfo( function( err : String, doc : String ) {
				Debug.assert( err == null );
				UnitTester.success();
				m.finished();
			});
		});
	}
	*/
	private function testDropInsert() {

		new Mongodb( "test", function( m : Mongodb ) {			 
			var fail = function() {
				UnitTester.failz();
				m.finished();
			}
			if( Config.config.verbose > 0 ) {
				trace( "begin db drop" );
			}

			m.mdb.dropDatabase( 
				m.wrap( function( result ) {
					trace( "Test DB dropped" );
					m.mdb.collection( "test", {},
						m.wrap( function( collection : Collection ) {
							collection.insert( {a:1}, {} );
							UnitTester.success();
							m.finished();
						})
					);
				}, fail ) 
			);
		});
	}
	/*
	private function testCount() {
		new Mongodb( "test", function( m : Mongodb ) {			 
			var fail = function() {
				UnitTester.failz();
				m.finished();
			}
			m.mdb.createCollection( "test3", { safe:1 },
				m.wrap( function( collection : Collection ) {
					collection.insert( { b:1 }, { safe:1 }, 
						m.wrap( function( data ) {
							collection.count( {}, 
								m.wrap( function( count ) {
									UnitTester.passOrFailz( count == 1 );
									m.finished();
								}, fail )
							);
						})
					);
				}, fail )
			);
		});
	}

	private function testFindOne() {
		new Mongodb( "test", function( m : Mongodb ) {			 
			m.mdb.createCollection( "test2", { safe:1 },
				m.wrap( function( collection : Collection ) {
					collection.insert( {b:1}, { safe:1 },
						m.wrap( function( data ) {
							collection.findOne( {b:1}, {fields:['_id', 'b'], safe:1},
								m.wrap( function( doc ) {
									UnitTester.passOrFailz( doc != null );
									m.finished();
								})
							);
						})
					);
				})
			);
		});
	}*/	
}
package framework.db.mongo;

import framework.db.mongo.ObjectId;
import framework.js.Node;
import framework.utils.Config;
import haxe.PosInfos;

/**
 * ...
 * @author Deano Calver
*/

typedef Options = Dynamic;

typedef Query = Dynamic;

typedef Callback = String -> Dynamic -> Void;


class Mongodb {		
	public var mdb(default, null) : Db;
	private var refCount : Int;
	public function new( dbname : String, connectCallback : Mongodb -> Void ) {
		var me = this;
		try {
			var fail = function() {
				trace( "connect to " + dbname + " failed" );
				me.finished();
			}
			
			Mongodb.connect( "mongo://" + Config.config.dbhost + ":" + Connection.DEFAULT_PORT + "/" + dbname, 
				me.wrap( function( _mdb : Db ) {
					if( Config.config.verbose > 0 ) {
						trace( "connected to " + dbname );
					}
					me.mdb = _mdb;
					me.refCount = 1;
					connectCallback( me );
				}, fail )
			);
		} catch ( e : String ) {
			trace( e );
			finished();
		}
	}
	
	public function finished() {
		--refCount;
		if ( mdb != null && refCount <= 0 ) {
			mdb.close();
			mdb = null;
		}
	}
	
	public function wrap( func : Dynamic, ?fail : Dynamic, ?pos : haxe.PosInfos ) {
		var me = this;
		me.refCount++;
		if ( fail == null ) {
			fail = function() { 				
				me.finished();
			}
		}	
		return function( err : String, result : Dynamic ) {
			try {
				if ( err != null ) {
					throw( err );
				}
				--me.refCount;
				func( result );
			} catch ( e : String ) {
				trace( pos.fileName + ":" + pos.lineNumber + ":Exception " + e + " in " + pos.className + "::" + pos.methodName, pos);
				fail();
			}
		};
	}

	public static function connect( ? url : String, callbak : Callback ) : Void {
		Node.require( "mongodb" ).connect( url, callbak );
	}

	public static function __init__() : Void {
		var mongodb = Node.require( "mongodb" );
		untyped db.mongo.Connection = mongodb.Connection;
		untyped db.mongo.Db = mongodb.Db;
		untyped db.mongo.Admin = mongodb.Admin;

		untyped db.mongo.ObjectId = mongodb.BSONPure.ObjectID;
	}
}
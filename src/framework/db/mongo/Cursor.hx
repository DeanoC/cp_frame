package framework.db.mongo;

import framework.db.mongo.Mongodb;
/**
 * ...
 * @author Deano Calver
 */

typedef Cursor = {
	function rewind() : Void;
	function toArray( callbak : Callback ) : Void;
	function each( callbak : Callback ) : Void;
	function count( callbak : Callback ) : Void;
	function sort( keyOrList : Dynamic, ?direction : Dynamic, ?callbac : Callback ) : Void;
	function limit( limit : Int, ?callbak : Callback ) : Void;
	function skip( skip : Int, ?callbak : Callback ) : Void ;
	function batchSize( batchSize : Int, ?callbak :Callback ) : Void;
	function limitRequest() : Int;
	function generateQueryCommand() : Dynamic;
	function formattedOrderClause() : Dynamic;
	function formatSortValue() : Dynamic;
	function nextObject( callbak : Callback ) : Void;
	function getMore( callbak : Callback ) : Void;
	function explain( callbak : Callback ) : Void;
	function streamRecords( args : Dynamic ) : Void;
	function close( ?callbak : Callback ) : Void;
	function isClosed() : Bool;
	
}
package framework.utils;

import haxe.Int32;
/**
 * add "using utils.Int32Helper;" to add functions to Int32 type
 * i.e. var a : Int32;
 * a.toHex();
 * @author Deano Calver
 */

class Int32Helper {
	static function toHex( v : Int32 ) : String {
		var ff = Int32.make(0, 0xFF);
		var a = Int32.toInt( Int32.and( Int32.shr( v, 24 ), ff ) );
		var b = Int32.toInt( Int32.and( Int32.shr( v, 16 ), ff ) );
		var c = Int32.toInt( Int32.and( Int32.shr( v,  8 ), ff ) );
		var d = Int32.toInt( Int32.and( Int32.shr( v,  0 ), ff ) );
		
		var t = StringTools.hex( d, 2 ) + 
				StringTools.hex( c, 2 ) +
				StringTools.hex( b, 2 ) +
				StringTools.hex( a, 2 );
		return t;
	}	
}
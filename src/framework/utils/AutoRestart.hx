package framework.utils;

class AutoStart {
	static function __init__() untyped {
#if nodejs
//	__js__("require.paths.unshift(__dirname);");
	__js__("var autoexit_watch = require(__dirname + '/autorestart').watch");
	__js__("var on_autoexit=function (filename) { }");
	__js__("autoexit_watch(__dirname,'.js', on_autoexit)");
	__js__("autoexit_watch(__dirname,'.json', on_autoexit)");
#end
	}
}
package framework.utils;

/**
 * ...
 * @author Deano Calver
 */

class DeepCopy {
	/**
	deep copy of anything 
	BUG: some structures seem to upset it at least in js 
		 (stack overrun) not sure why yet
	**/
	public static function copy<T>( v:T ) : T {
		if (!Reflect.isObject(v))  {
			// simple type
		
			return v;
		} else if ( Std.is( v, Array ) )  {
			// array
			var r = Type.createInstance(Type.getClass(v), []);
			untyped {
				for( ii in 0...v.length )
					r.push(copy(v[ii]));
			}
			
			return r;
		} else if( Type.getClass(v) == null ) {
			// anonymous object
			var obj : Dynamic = { };
			for( ff in Reflect.fields(v) )
				Reflect.setField(obj, ff, copy(Reflect.field(v, ff)));
			
			return obj;
		} else {
			// class
			var obj = Type.createEmptyInstance(Type.getClass(v));
			for( ff in Reflect.fields(v) )
				Reflect.setField(obj, ff, copy(Reflect.field(v, ff)));
		
			return obj;
		}
		
		return null;
	}
}
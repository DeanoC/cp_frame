package framework.utils;

/**
 * ...
 * @author Deano Calver
 */

class BinarySearch {
	// data is assumed to be sorted prior to calling
	static public function search( 	data : Array<Int>,
									value : Int,
									start_index : Int = 0, 
									end_index : Int = -1 ) : Bool {										
		if ( end_index == -1 ) {
			end_index = data.length;
		}
		var middle_index : Int = (start_index + end_index) >> 1;
		var was_found : Bool = false;
		
		if(start_index < end_index) {
			if(data[middle_index] == value) {
				return true;
			} else {
				if(data[middle_index] > value) {
					was_found = search(data, value, start_index, middle_index);
				} else {
					was_found = search(data, value, middle_index + 1, end_index);
				}
			}
		}
		return was_found;
	}
	
	static public function union( a : Array<Int>, b: Array<Int> ) : Array<Int> {
		var r = new Array<Int>();
		for ( i in a ) {
			if ( BinarySearch.search(b, i, 0, b.length ) == true ) {
				r.push(i);
			}
		}
		return r;
	}
}
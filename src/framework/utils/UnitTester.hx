package ;
import haxe.PosInfos;
import haxe.Public;

/**
 * ...
 * @author Deano Calver
 */
private class TestSubject implements Public {
	var totalTests : Int;
	var tests : Int;
	var failz : Int;
	var clas : Class<Dynamic>;
	var tearDownMethod : String;
	
	function new( _cl : Class<Dynamic> ) {
		tests = 0;
		failz = 0;
		clas = _cl;
		
	}
}


class UnitTester {
	private static var testSubjects : Hash<TestSubject> = new Hash<TestSubject>();
	
	public static function addTestSubject( clas : Dynamic ) {
		var cl = Type.getClass(clas);
		var subject = new TestSubject( cl );
		testSubjects.set( Type.getClassName(cl), subject );
		trace( "Test Subject : "+Type.getClassName(cl) );

		// BUG: Reflect.isFunction appears to have problems in the cpp target
		var fields = Type.getInstanceFields(cl);
		for ( f in fields ){
			var fname = f;
			var field = Reflect.field(clas, f);
			if ( fname == "setupUnitTests" /*&& Reflect.isFunction(field)*/ ) {
				Reflect.callMethod( clas, field, new Array() );
			} else if ( fname == "tearDownUnitTests" /*&& Reflect.isFunction(field)*/ ) {
				subject.tearDownMethod = field;
			} else if ( StringTools.startsWith(fname, "test") /*&& Reflect.isFunction(field)*/ ) {
				subject.tests++;
			}
		}
		subject.totalTests = subject.tests;
		for ( f in fields ) {
			var fname = f;
			var field = Reflect.field(clas, f);
			if ( StringTools.startsWith(fname, "test") /*&& Reflect.isFunction(field)*/ ) {
				Reflect.callMethod( clas, field, new Array() );
			}			
		}
	}
	
	public static function endCheck() : Void {
		for ( subject in testSubjects ) {
			if ( subject.tests != 0 ) {
				trace( Type.getClassName(subject.clas) + " did not complete " + subject.tests + " out of " + subject.totalTests + " unit tests" );
			}
		}
	}
	
	
	public static function success( ?pos : PosInfos) : Void {
		var subject = testSubjects.get( pos.className );
		Debug.assert( subject != null );
		subject.tests--;

		if ( subject.tests == 0 ) {
			lastDance( subject );
		}
	}
	
	public static function failz( ?pos : PosInfos ) {
		var subject = testSubjects.get( pos.className );
		if ( subject == null ) {
			// a Debug.assert not associated with a unit test
			return;
		}
		subject.tests--;
		subject.failz++;
		haxe.Log.trace("Unit test fail", pos);
		if ( subject.tests == 0 ) {
			lastDance( subject );
		}
	}
	
	public static function passOrFailz( b : Bool, ?pos: PosInfos ) {
		if( b == true ) {
			UnitTester.success( pos );
		} else {
			UnitTester.failz( pos );
		}		
	}
	
	private static function lastDance( subject : TestSubject ) {
		if ( subject.failz == 0 ) {
			trace( Type.getClassName(subject.clas) + " has passed all " + subject.totalTests + " unit tests" );
		} else {
			trace( Type.getClassName(subject.clas) + " failed " + subject.failz + " out of " + subject.totalTests + " unit tests" );
		}
		if( subject.tearDownMethod != null && subject.tearDownMethod.length > 0 ) {
			Reflect.callMethod( subject.clas, subject.tearDownMethod, new Array() );
		}
	}
}
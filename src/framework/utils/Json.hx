package framework.utils;

#if nodejs
import js.Node;
#else
import framework.third_party.hxjson2.JSON;
#end

/**
 * ...
 * @author Deano Calver
 * 
 * Provide a cross platform Json decoder, we have a pure hx version to fallback
 * on but there may be native versions which *should* be faster
 * Node.js for example using Google V8 json decoder
 */
class Json {

	public static function encode( o : Dynamic ) : String {
#if nodejs
		return Node.stringify( o );
#else
		return JSON.encode( o );
#end
	}

	public static function decode( s : String ) : Dynamic {
#if nodejs
		return Node.parse( s );
#else
		return JSON.decode( s );
#end
	}
}